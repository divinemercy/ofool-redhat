<?php

namespace Ofool\GlossaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/roi")
     */
    public function indexAction()
    {
        //return $this->render('OfoolGlossaryBundle:Default:index.html.twig');
        return new \Symfony\Component\HttpFoundation\Response('---------------------');
    }
}
