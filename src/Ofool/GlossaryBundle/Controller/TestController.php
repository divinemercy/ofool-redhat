<?php

namespace Ofool\GlossaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Ofool\GlossaryBundle\Helper\Lib as GlossaryLib;
use Ofool\GlossaryBundle\Helper\Import as GlossaryImport;

class TestController extends Controller {

    /**
     * @Route("/test")
     */
    public function indexAction() {

//        $import = new Import($this->container);
//        $import->loadGLossaryCategories();
//        
//        //Test de creation de categorie de glossaire
//        $glossaryLib = new GlossaryLib($this->container);
//        $glossaryLib->createCategory();
//        //Test d'affichage de la liste de categories crée
//        $glossaryLib = new GlossaryLib($this->container);
//        $result = $glossaryLib->findAllCategories();
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        
//        
        //Test de recherche 1
//        $glossaryLib = new GlossaryLib($this->container);
//        $result = $glossaryLib->recherche();
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//
//        return new \Symfony\Component\HttpFoundation\Response('---only for testing purpose------------------');
        // Import Category Glossary Importation of Category Decommenter les 04 premières lignes à partir d'ici
        //$fileName = __DIR__ . '/../data/Final Glossary Category.xlsx';        
        //$import = new GlossaryImport($this->container);
        //       $data = $import->excelToArrayCategory($fileName);
        //     $import->importGLossaryCategories($data);
//        
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        
        // Import Term Glossary        Importation of Term Glossary decommenter les 04 premières lignes à partir d'ici
//        $fileNameTerm = __DIR__ . '/../data/Final Glossary.xlsx';
//        $data = $import->excelToArrayTerm($fileNameTerm);
//        set_time_limit(1500);
//                $import->importGLossaryTerm($data);
//        set_time_limit(30000);
//        ini_set('memory_limit', '-1');


//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        



        return new \Symfony\Component\HttpFoundation\Response('---Test 1------------------');
    }
    
    /**
     * @Route("/import")
     */
    public function importAction() {

//        $import = new Import($this->container);
//        $import->loadGLossaryCategories();


        // Import Category Glossary Importation of Category Decommenter les 04 premières lignes à partir d'ici
        $fileName = __DIR__ . '/../data/Final Glossary Category.xlsx';
        $import = new GlossaryImport($this->container);
        $data = $import->excelToArrayCategory($fileName);
        $import->importGLossaryCategories($data);
        
        echo '---import category terminé------------------<br><br>';
//        
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        
        // Import Term Glossary        Importation of Term Glossary decommenter les 04 premières lignes à partir d'ici
//        $fileNameTerm = __DIR__ . '/../data/Final Glossary.xlsx';
//        $data = $import->excelToArrayTerm($fileNameTerm);
//        set_time_limit(1500);
//                $import->importGLossaryTerm($data);
//        set_time_limit(30000);
//        ini_set('memory_limit', '-1');


//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        



        return new \Symfony\Component\HttpFoundation\Response('---Test 1------------------');
    }

}
