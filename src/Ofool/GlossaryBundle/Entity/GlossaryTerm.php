<?php

namespace Ofool\GlossaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * GlossaryTerm
 *
 * @ORM\Table(name="glossary_term")
 * @ORM\Entity(repositoryClass="Ofool\GlossaryBundle\Repository\GlossaryTermRepository")
 */
class GlossaryTerm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
          
    /**
     * @var string
     *
     * @ORM\Column(name="definition", type="text")
     */
    private $definition;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;   
    
    
    /**
     * @ManyToOne(targetEntity="GlossaryCategory")     
     */
    private $category;    


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GlossaryTerm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set definition
     *
     * @param string $definition
     *
     * @return GlossaryTerm
     */
    public function setDefinition($definition)
    {
        $this->definition = $definition;

        return $this;
    }

    /**
     * Get definition
     *
     * @return string
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return GlossaryTerm
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set category
     *
     * @param \Ofool\GlossaryBundle\Entity\GlossaryCategory $category
     *
     * @return GlossaryTerm
     */
    public function setCategory(\Ofool\GlossaryBundle\Entity\GlossaryCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Ofool\GlossaryBundle\Entity\GlossaryCategory
     */
    public function getCategory()
    {
        return $this->category;
    }
}
